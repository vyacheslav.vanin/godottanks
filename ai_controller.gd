extends Node

@export var SPEED = 5;
@onready var parent : CharacterBody3D = $".."
@onready var main = get_node("/root").get_child(0)

var _directions : Array[Vector3] = [
	Vector3(1,0,0),
	Vector3(-1,0,0),
	Vector3(0,0,1),
	Vector3(0,0,-1),
	Vector3(1,0,1).normalized(),
	Vector3(1,0,-1).normalized(),
	Vector3(-1,0,1).normalized(),
	Vector3(-1,0,-1).normalized(),
]

var input_dir = Vector3()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	_on_direction_timer_timeout()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	for i in range(parent.get_slide_collision_count()):
		var collision = parent.get_slide_collision(i)
		var collider = collision.get_collider()
		if collider == null:
			continue

		if collider.is_in_group("wall"):
			$DirectionTimer.start()
			_on_direction_timer_timeout()

	var direction = input_dir
	if direction.length_squared():
		parent.look_at(parent.position + direction)
		
	if direction:
		parent.velocity.x = direction.x * SPEED
		parent.velocity.z = direction.z * SPEED
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, SPEED)
		parent.velocity.z = move_toward(parent.velocity.z, 0, SPEED)

	parent.move_and_slide()

func _on_direction_timer_timeout():
	input_dir = _directions[randi() % _directions.size()]


func _on_shoot_timer_timeout():
	parent.shoot()
