extends Node

@export var SPEED = 5;
@onready var parent = $".."
@onready var main = get_node("/root").get_child(0)

func _ready():
	pass

func _process(delta):
	pass
	
func _physics_process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		parent.shoot()

	var input_dir = Input.get_vector( "ui_left", "ui_right", "ui_up", "ui_down")
	var direction = Vector3(input_dir.x, 0, input_dir.y)
	if direction.length_squared():
		parent.look_at(parent.position + direction)
		
	if direction:
		parent.velocity.x = direction.x * SPEED
		parent.velocity.z = direction.z * SPEED
	else:
		parent.velocity.x = move_toward(parent.velocity.x, 0, SPEED)
		parent.velocity.z = move_toward(parent.velocity.z, 0, SPEED)

	parent.move_and_slide()
