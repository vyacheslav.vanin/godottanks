extends Node

var spawner = null

func initialize(_spawner):
	spawner = _spawner

func _exit_tree():
	spawner.active_spawned_count -= 1
