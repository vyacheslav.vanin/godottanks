extends GPUParticles3D


# Called when the node enters the scene tree for the first time.
func _ready():
	emitting = true
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func initialize(_position):
	position = _position

func _on_destruction_timer_timeout():
	queue_free()
