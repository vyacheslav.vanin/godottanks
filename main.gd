extends Node3D

var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.destroyed.connect(_on_player_destroyed)
	$EnemySpawner.spawned.connect(_on_spawner_spawned)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and $Gui/ColorRect.visible:
		# This restarts the current scene.
		get_tree().reload_current_scene()


func _on_enemy_destroyed(destroyer, enemy):
	# handle enemy death by collision with player
	if destroyer.is_in_group("player"):
		_on_player_destroyed(enemy)
	destroyer.queue_free()

	score += 1
	$Gui/Score/count.text = str(score)
 

func _on_player_destroyed(destroyer):
	$Gui/ColorRect/died.show()
	$Gui/ColorRect.show()

func _on_spawner_spawned(spawner):
	spawner.spawned.connect(_on_enemy_spawner_spawned)

func _on_enemy_spawner_spawned(spawned_object):
	spawned_object.destroyed.connect(_on_enemy_destroyed.bind(spawned_object))
