extends Node3D

@export var spawn_scene: PackedScene
@export var max_to_spawn: int = 0
@export var max_active: int = 5
@export var batch_size: int = 1
@export var period : float = 1
@export var project_on_ground : bool = true
@export_flags_2d_physics var collision_mask = 0xffffffff
signal spawned(spawned_object)

@onready var main = get_node("/root").get_child(0)
@onready var timer = $Timer

var active_spawned_count = 0
var spawned_count = 0

func _ready():
	randomize()
	if period > 0:
		timer.wait_time = period
		timer.start()
	else:
		_on_timer_timeout()

func _process(delta):
	pass

func _random_vector3(pos: Vector3, scale_: Vector3):
	scale_ /= 2
	var x_min = pos.x - scale_.x
	var x_max = pos.x + scale_.x
	var y_min = pos.y - scale_.y
	var y_max = pos.y + scale_.y
	var z_min = pos.z - scale_.z
	var z_max = pos.z + scale_.z
	return Vector3(
		lerp(x_min, x_max, randf()),
		lerp(y_min, y_max, randf()),
		lerp(z_min, z_max, randf()),
	)

func _raycast_down(pos: Vector3, length: float, direction: Vector3 = Vector3(0, -1, 0)):
	var space = get_world_3d().direct_space_state

	var query = PhysicsRayQueryParameters3D.create(
		pos, pos + length * direction, collision_mask, [self])
	var result = space.intersect_ray(query)
	if result:
		return result.position
	return pos

func _on_timer_timeout():
	var to_spawn = batch_size

	while (max_to_spawn == 0 or spawned_count < max_to_spawn) \
			and active_spawned_count < max_active \
			and to_spawn > 0:
		var notifier = Node.new()
		notifier.name = "_spawner_destruction_notifier"
		notifier.set_script(load("res://spawner_deletion_notifier.gd"))
		notifier.initialize(self)

		spawned_count += 1
		active_spawned_count += 1
		to_spawn -= 1
		var spawn = spawn_scene.instantiate()
		spawn.position = _random_vector3(position, scale)
		if project_on_ground:
			spawn.position = _raycast_down(spawn.position, 1000)
		spawn.add_child(notifier)
		main.add_child.call_deferred(spawn)
		spawned.emit(spawn)
