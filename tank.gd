extends CharacterBody3D


@export var projectile_scene: PackedScene
@export var explosion_scene: PackedScene
@export var color : Color

signal destroyed(destroyer_object)

@onready var main = get_node("/root").get_child(0)
const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


func _ready():
	$Detector.collision_layer = collision_layer
	$Detector.collision_mask = collision_mask
	$MeshInstance3D.get_surface_override_material(0).albedo_color = color

func _physics_process(delta):
	pass

func _on_area_3d_body_entered(body):
	if body.is_in_group("projectile") or body.is_in_group("tank"):
		var explosion = explosion_scene.instantiate()
		explosion.initialize(position)
		main.add_child(explosion)
		
		destroyed.emit(body)
		queue_free()

func shoot():
	var bullet = projectile_scene.instantiate()
	var direction = transform.basis * Vector3(0, 0, -1).normalized()
	bullet.initialize(position+direction, direction, collision_layer)
	main.add_child(bullet)
