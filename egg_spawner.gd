extends Node3D

@export var delay_sec : float = 1
## Scene displayed before spawn
@export var wait_scene: PackedScene
## Scene wich will be spawnd after dalay
@export var spawn_scene: PackedScene
signal spawned(spawned_object)

@onready var main = get_node("/root").get_child(0)
var wait_scene_instance = null

# Called when the node enters the scene tree for the first time.
func _ready():
	wait_scene_instance = wait_scene.instantiate()
	add_child(wait_scene_instance)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_timer_timeout():
	wait_scene_instance.queue_free()
	var spawn = spawn_scene.instantiate()
	spawn.position = position
	main.add_child(spawn)
	spawned.emit(spawn)
	spawn.tree_exited.connect(_on_spawn_destroed)

func _on_spawn_destroed():
	queue_free()
