extends CharacterBody3D


const SPEED = 15.0

func initialize(start_position, direction, collision_layer_):
	top_level = true
	velocity = direction * SPEED
	position = start_position
	collision_layer = collision_layer_
	collision_mask = 0

func _physics_process(delta):
	move_and_slide()
